# import libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
import category_encoders as ce
from sklearn.linear_model import LogisticRegression
from joblib import load
from pathlib import Path


path = Path( "data/weather_test.csv")  

df = pd.read_csv(path)

smaple = df.head(2)

print(smaple)

model_path = Path( "model.joblib")

model = load( model_path)
