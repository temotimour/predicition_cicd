FROM python:3.9
LABEL Maintainer="University of Canberra"
COPY . /prediction
WORKDIR /prediction
RUN pip install -r requirements.txt
CMD [ "python" ,"prediction.py"]